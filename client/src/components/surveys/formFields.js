const formFields = [
  {
    label: 'Survey Title',
    name: 'title',
    noValueError: 'Provide a survey title',
  },
  {
    label: 'Subject Line',
    name: 'subject',
    noValueError: 'Provide a survey subject',
  },
  { label: 'Email Body', name: 'body', noValueError: 'Provide the email body' },
  {
    label: 'Recipient list',
    name: 'recipients',
    noValueError: 'Provide valid list of recipients',
  },
];

export default formFields;
