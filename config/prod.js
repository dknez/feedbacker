module.exports = {
  googleClientId: process.env.GOOGLE_CLIENT_ID,
  googleClientSecret: process.env.GOOGLE_CLIENT_SECRET,
  mongoUri: process.env.MONGO_URI,
  cookieKey: process.env.COOKIE_KEY,
  stripePublishableKey: process.env.REACT_APP_STRIPE_KEY,
  stripeSecretKey: process.env.REACT_APP_STRIPE_SECRET_KEY,
  sendGridKey: process.env.SENDGRID_KEY,
  redirectDomain: process.env.REDIRECT_DOMAIN,
};
